#+TITLE: Authoritative reasoning

Concept from [[file:exploiting_rdfs_and_owl_for_integrating_heterogeneous_large_scale_linked_data_corpora.org][Exploiting RDFS and OWL for Integrating Heterogeneous, Large-Scale, Linked Data Corpora (2011)]].

#+BEGIN_QUOTE
... we  currently have no means of distinguishing “good” third-party
contributions from “bad” third-party contributions.  We call this more
conservative form of reasoning authoritative  reasoning, which only considers
authoritatively published terminological data ...
#+END_QUOTE
