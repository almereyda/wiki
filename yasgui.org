#+TITLE: Yasgui
#+ROAM_KEY: https://triply.cc/docs/yasgui

A nice GUI for SPARQL queries.

Used by Linked Data portal of geo.admin: https://ld.geo.admin.ch/sparql/
