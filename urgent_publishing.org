#+TITLE: Urgent Publishing

A [[https://networkcultures.org/blog/publication/here-and-now-explorations-in-urgent-publishing/][publication]] by the [[file:institute_of_network_cultures.org][Institute of Network Cultures]] that explores /urgent publishing/:

- speed :: How to accelerate the publishing process?
- positioning :: How to get your publication in the right hands?
- quality :: How to ensure quality of published content?

Also talks about /Relations/, /Trust/ and /Remediation/ (how can the content be reused - how can life of publication be prolonged?). A lot of ideas that fit very well with the [[file:rdf.org][Linked Data]] way of thinking.

Much inspiration to be taken for how to an [[file:activitypub.org][ActivityPub]] based publishing system could look.
