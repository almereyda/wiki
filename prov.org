#+TITLE: PROV
#+ROAM_ALIAS: "Provenance interchange"

PROV is a data model and [[file:rdf.org][RDF]] [[file:ontologies.org][ontology]] to describe the origin of a piece of data by capturing the various agents, activities and other pieces of data that were involved in the creation of the data.

PROV is the result of a [[file:w3c.org][W3C]] working group ([[www.w3.org/2011/prov/][Provenance Working Group]]). They published a number of documents and specifications including:

- [[https://www.w3.org/TR/prov-primer/][PROV Primer]]: An introduction
- [[http://www.w3.org/TR/prov-dm/][PROV-DM]]: Description of the PROV data model
- [[http://www.w3.org/TR/prov-o/][PROV-O]]: An [[file:rdf.org][RDF]] [[file:ontologies.org][ontology]] for using PROV
- [[https://www.w3.org/TR/prov-dc/][PROV-DC]]: A mapping from [[file:dublin_core.org][Dublin Core]] terms to PROV-O

See the overview document ([[http://www.w3.org/TR/prov-overview/][PROV Overview]]) for a complete list of all published documents.

* Usage

PROV seems to be the primary method of describing provenance, at least there is plenty of academic discussion and papers describing the use of PROV in various field (see [[https://link.springer.com/chapter/10.1007/978-3-030-52829-4_12#Sec2][Data Provenance (2020)]]). A study from 2016 indicates an wide reference in academia but less direct usage in published data ([[http://provenanceweek.org/2016/p3yl/papers/paper_89.pdf][Measuring PROV Provenance on the Web of Data (2016)]]).

** Caching

Can PROV be used to describe content-addressed caches of data?

** Signing

Can PROV be used to describe cryptographic signatures of data?
