#+title: Geometa Lab

Research lab at the Institute for Software (IFS) at OST (formerly Hochschule für Technick Rapperswil).

The lab does research on [[file:openstreetmap.org][OpenStreetMap]], GIS and database systems.

Interesting and relavent research for [[file:index.org][openEngiadina]]:

- [[https://geometalab.tumblr.com/post/169813788729/vector-tiles-the-web-mapping-technology-of-the][Vector Tiles]]
- Public-OpenStreetMap-Partnership (POP)
