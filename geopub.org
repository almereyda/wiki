#+TITLE: GeoPub

GeoPub is a client for the [[file:index.org][openEngiadina]] platform. It is intended to be usable
as a content management system for open local knowledge.

* [[https://gitlab.com/openengiadina/geopub][Repository]]
