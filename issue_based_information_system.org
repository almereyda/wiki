#+TITLE: Issue-based information system
#+ROAM_ALIAS: IBIS

IBIS is a formal argumentation system for dealing with /wicked/ problems - problems that are ill-defined and have no clear answer - most problems.

The problem is stated as an /issue/ that describes the problem (mostly stated as a question). Issues are answered with different /positions/ (possible answers to question). /Arguments/ can can be posted supporting or objecting positions. New issues can be posted as refinement or replacement of existing issues. The discussion and handling of the issue results in a graph.

IBIS seems to be very well suited for an interaction-model similar to a social network. Users can create a new issue, post positions and arguments and reference further content. IBIS seems to be a perfect fit for RDF. A RDF vocabulary exists: [[https://privatealpha.com/ontology/ibis/1##osd#osds][The IBIS (bis) vocabulary]]. See also [[http://www.cs.hut.fi/Opinnot/T-93.850/2005/Papers/gIBIS1988-conklin.pdf][gIBIS]] which explores tools for doing IBIS.

It would be interesting to build a IBIS system on [[file:index.org][openEngiadina]].

* pol.is

pol.is is a platform for public discourse. Positions can be posted and users can agree or disagree to positions. Idea is similar to IBIS, slightly Tinder-ized and mixed in with /artificial intelligence/.

pol.is was used with success in [[https://blog.pol.is/pol-is-in-taiwan-da7570d372b5][Taiwan]].

* See also
** [[https://en.wikipedia.org/wiki/Issue-based_information_system][Wikipedia]]

Wikipedia article on IBIS.

** Compedium

Compedium was /the/ IBIS software. It was abandoned and resurfaced on GitHub as
[[https://github.com/CompendiumNG/CompendiumNG][CompediumNG]]. CompediumNG also seems defunct now.
