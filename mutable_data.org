#+TITLE: Mutable data

[[file:content_addressed_storage.org][Content-addressed storage]] solves the problem of [[file:availability.org][availability]]. However it imposes
immutability of data.

Mutability is required in dynamic systems. How can it be implemented securely
and decentralized?

* Previous work
** [[file:datashards.org][Datashards]]  / [[https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org][Crystal]]

Conflicts can arise when multiple writers write uncoordinated. This is
considered to be a necessary trade-off (see [[https://en.wikipedia.org/wiki/CAP_theorem][CAP theorem]]).

They do mention eventual consistency as solution, however consider the cost of
such systems too high:

#+BEGIN_QUOTE
For systems such as distributed social networks, the cost of eventual consistency is simply too high, but for other systems, the cost might be the only way forward while preserving core design decisions.

--- [[https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org][Spritely Crystal]]
#+END_QUOTE

Maybe the cost of eventual consistency is not as high when using [[file:commutative_replicated_data_types.org][CRDT]]s.
** [[file:tahoe_lafs.org][Tahoe-LAFS]]

Similar to [[file:datashards.org][Datashards]] they require coordination between writers.

See [[https://tahoe-lafs.readthedocs.io/en/latest/specifications/mutable.html#the-prime-coordination-directive-don-t-do-that][The Prime Coordination Directive: “Don’t Do That”]].
