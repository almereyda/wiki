#+TITLE: Institute of Network Cultures
#+ROAM_ALIAS: INC

A research institute focusing on digital publishing and digital counter culture. [[file:urgent_publishing.org][Urgent Publishing]] is a term coined by the INC.

* See also
** [[https://networkcultures.org/][Home page]]
