#+TITLE: Vegvisir

Research project at Cornell University.

Proposes the usage of [[file:commutative_replicated_data_types.org][CRDT]] as a partition-tolerant blockchain.

Interesting contribution seems to be adding Tamperproofness and Authenticity to CRDTs.

* See also
** [[https://vegvisir.cs.cornell.edu/][Vegvisir]]

Project home page.

** [[https://vegvisir.cs.cornell.edu/html/files/papers/vegvisir-paper.pdf][Vegvisir: A Partition-Tolerant Blockchain for the Internet-of-Things (2018)]]

Paper describing the project and scheme.
