#+TITLE: Datalog

Datalog is a declarative query language.

* Further Reading

** Books & Papers

- [[https://personal.utdallas.edu/~gupta/courses/acl/papers/datalog-paper.pdf][What You Always Wanted to Know About Datalog (And Never Dared to Ask) (1989)]] :: A good first introduction to Datalog. Includes Deatalog semantics, relation to Prolog and relational algebra as well as illustrating optimization techniques used by Datalog compilers.
- [[http://libgen.rs/book/index.php?md5=CCBD7C8C89D6EB56219E030CD81BFDD2][Logic Programming and Databases]] :: An in-depth treatment of Datalog describing the semantics, relationship to relational algebra and Prolog. Gives an overview of inference algorithms as well as extensions.
- [[https://cs.uwaterloo.ca/~david/fdb/][Foundation of Databases]] :: Course and Book on Databases highlighting the logical foundations including Datalog.

** Blogs

- [[https://x775.net/2019/03/18/Introduction-to-Datalog.html][Introduction to Datalog]] :: A very nice introduction which goes into the different semantics of Datalog.

** Implementations

*** Clojure

The Clojure community has embraced Datalog as a query language. The makers of the language have developed a database that uses Datalog as query language ([[https://www.datomic.com/][Datomic]]). Other (open-source) implementations exist:

- [[https://github.com/tonsky/datascript][datascript]] :: A Clojure/ClojureScript in-memory Datalog implementation.
- [[https://github.com/replikativ/datahike][datahike]] :: A durable Datalog database that started as a datascript port.

*** Other

- [[https://github.com/c-cube/datalog][datalog (OCaml)]] :: An in-memory OCaml implementation of Datalog.
- [[https://docs.racket-lang.org/datalog/index.html][datalog (Racket)]] :: A Racket implementation of Datalog.

** Extensions

A selection of extensions and applications of Datalog.

- [[https://drops.dagstuhl.de/opus/volltexte/2015/5017/pdf/7.pdf][Yedalog]] :: A Datalog implementation for massive scale data (compiles to MapReduce). One of the authors (Mark S. Miller) was at ActivityPub Conference 2019 and pointed me to this work.
- [[https://mobisocial.stanford.edu/papers/vldb14s.pdf][Distributed SociaLite: A Datalog-Based Language for Large-Scale Graph Analysis]] :: Datalog extension allowing parallel/distributed computation.
